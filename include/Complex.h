#include <string>
#include <iostream>
using namespace std;

class Complex {
private:
	string nazwa;
	double real;
	double imag;
	double mod;
	double arg;
public:
		Complex() {
		nazwa="Brak nazwy";
		real=0.0;
		imag=0.0;
		mod = modul();
		arg = kat();
		cout << "utworzono pusty" << endl;
		}

		Complex(string nazwa, double real, double imag) {
		this->nazwa=nazwa;
		this->real=real;
		this->imag=imag;
		mod = modul();
		arg = kat();
		cout << "utworzono 3 parametrowy" << endl;
		}

		void info() {
			cout << "Nazwa:" << nazwa << endl;
			cout << "Real:" << real << endl;
			cout << "Imag:" << imag << endl;
			cout << "Modul:" << mod << endl;
			cout << "Kat:" << arg << endl;
			cout << "Adres : 0x" << this << endl;
			}
		void info_bez_nazwy() {
			cout << "Real:" << real << endl;
			cout << "Imag:" << imag << endl;
			cout << "Modul:" << mod << endl;
			cout << "Kat:" << arg << endl;
			cout << "Adres : 0x" << this << endl;
			}

		double modul() const {
			return sqrt(pow(real, 2.0) + pow(imag, 2.0));
		}
		double kat(){
			return atan2(imag, real) *180.0/M_PI;
		}

		string Get_name(){return nazwa;};
		double Get_real(){return real;};
		double Get_imag(){return imag;};
};



