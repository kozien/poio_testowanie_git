#include <gtest/gtest.h>

#include <iostream>
#include <math.h>
#include <string>

using namespace std;
#include <../include/Complex.h>

TEST(ComplexTest, ZeroInitialized){
	Complex p1;
	EXPECT_EQ(0.0, p1.Get_real());
}

TEST(ComplexTest, FullInitialized){
	Complex p2("p2", 1.0, 1.0);
	EXPECT_EQ(1, p2.Get_real());
	EXPECT_EQ(1, p2.Get_imag());
	EXPECT_EQ("p2", p2.Get_name());
}

int main(int argc, char* argv[]){
	testing::InitGoogleTest(&argc, argv);
	cout << "Starting test..." << endl << flush;
	int res = RUN_ALL_TESTS();
	cout << endl << "RUN_ALL_TESTS(): " << res;
	return 0;
}

